package com.test.gpu

/**
 * Created by seather on 8/11/15.
 */
object Main extends App {
  println("Testing kernels on the GPU via OpenCL and comparing to standard threads")
  println()
  println("========== Pure Scala ==========")
  HelloScala.main(args)
  println()
  println("========== JOCL ==========")
  HelloJOCL.main(args)
  println()

  println("DONE")

}
