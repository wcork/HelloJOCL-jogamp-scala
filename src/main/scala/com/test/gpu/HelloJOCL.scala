package com.test.gpu

import java.nio.FloatBuffer

import com.jogamp.opencl._
import com.jogamp.opencl.util.CLProgramConfiguration

import scala.util.Random

class HelloJOCL(ctx: CLContext) {

  // select fastest device
  lazy val device = {
    val dev = ctx.getMaxFlopsDevice
    println("using " + dev + " CVersion: " + dev.getCVersion +
      " MaxComputeUnits: " + dev.getMaxComputeUnits +
      " DoubleFPConfig: " + dev.getDoubleFPConfig)
    if (dev.getType != CLDevice.Type.GPU) {
      printf("WARNING: GPU not selected!")
    }
    dev
  }

  // create command queue on device.
  var properties = CLCommandQueue.Mode.PROFILING_MODE
  val queue = device.createCommandQueue(properties)

  // Length of arrays to process
  val elementCount = 4194304

  // Local work size dimensions
  val localWorkSize = math.min(device.getMaxWorkGroupSize, 256)

  // rounded up to the nearest multiple of the localWorkSize
  val globalWorkSize = roundUp(localWorkSize, elementCount)

  // load sources, create and build program
  val program = ctx.createProgram(classOf[HelloJOCL].getResourceAsStream("/VectorAdd.cl")).build()

  // A, B are input buffers, C is for the result
  // fill input buffers with random numbers
  // (just to have test data; seed is fixed -> results will not change between runs).

  import com.jogamp.opencl.CLMemory.Mem.{READ_ONLY, WRITE_ONLY}

  val clBufferA = fillBuffer(ctx.createFloatBuffer(globalWorkSize, READ_ONLY), 12345)
  val clBufferB = fillBuffer(ctx.createFloatBuffer(globalWorkSize, READ_ONLY), 67890)
  val clBufferC = ctx.createFloatBuffer(globalWorkSize, WRITE_ONLY)

  // get a reference to the kernel function with the name 'VectorAdd'
  // and map the buffers to its input parameters.
  lazy val kernel = {
    println("used device memory: " +
      (clBufferA.getCLSize + clBufferB.getCLSize + clBufferC.getCLSize) / 1024 / 1024 + "MiB")

    println("localWorkSize: " + localWorkSize + ", globalWorkSize: " + globalWorkSize)

    program.createCLKernel("VectorAdd")
      .putArgs(clBufferA, clBufferB, clBufferC).putArg(elementCount)
  }

  def fillBuffer(clBuf: CLBuffer[FloatBuffer], seed: Int) = {
    def nextFloats(size: Int) = Array.fill(size)(Random.nextFloat() * 100)

    val buffer = clBuf.getBuffer
    Random.setSeed(seed)
    buffer.put(nextFloats(buffer.remaining)).rewind()
    clBuf
  }

  def roundUp(groupSize: Int, globalSize: Int) = {
    val r = globalSize % groupSize
    if (r == 0) globalSize
    else globalSize + groupSize - r
  }

  def run() = {
    // asynchronous write of data to GPU device,
    // followed by blocking read to get the computed results back.
    val events: CLEventList = new CLEventList(50)
    queue
      .putWriteBuffer(clBufferA, false)
      .putWriteBuffer(clBufferB, false)
      .put1DRangeKernel(kernel, 0, globalWorkSize, localWorkSize, events)
      .putReadBuffer(clBufferC, true)
    val ret = clBufferC.getBuffer //BLOCKING READ

//    val it = events.iterator()
//    while (it.hasNext) {
//      val event = it.next()
//      val baseTime = event.getProfilingInfo(CLEvent.ProfilingCommand.QUEUED)
//      println("\tEvent Name:" + event.toString)
//      println("\tQUEUED Time: " + baseTime)
//      println("\tSUBMIT Time: +" + (event.getProfilingInfo(CLEvent.ProfilingCommand.SUBMIT) - baseTime) / 1e3 + " µs")
//      println("\tSTART Time: +" + (event.getProfilingInfo(CLEvent.ProfilingCommand.START) - baseTime) / 1e3 + " µs")
//      println("\tEND Time: +" + (event.getProfilingInfo(CLEvent.ProfilingCommand.END) - baseTime) / 1e3 + " µs")
//    }


    ret.rewind()
    ret
  }

}

object HelloJOCL {

  def main(args: Array[String]) {
    val bsCall = System.nanoTime
    println("==== ScalaCL ====")
    val ctx = CLContext.create()
    println("created " + ctx)

    try {
      val hello = new HelloJOCL(ctx)

      for (i <- 1 to 10) {
        val startTime = System.nanoTime
        val ret = hello.run()
        val endTime = System.nanoTime

        if (i == 1) {
          // print first few elements of the resulting buffer to the console.
          println("a+b=c results snapshot: ")
          for (i <- 0 until 10) {
            print(ret.get() + ", ")
          }
          println("...; " + ret.remaining + " more")
        }
        println("computation took %2d: %d micro sec" format(i, (endTime - startTime) / 1000))
      }

    } finally ctx.release()
  }
}