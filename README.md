HelloJOCL-jogamp
===============

This is a simple test of the jogamp.jocl interface to OpenCL with scala.

A simple comparison between running on native scala serially, parallel collections and jocl is made.

### How to use:
build with
```
-> ./gradlew build
```
run with
```
-> ./gradlew run
```

Notes
-----
This is a re-implementation of the HelloWorld from jogamp.jocl in Java.